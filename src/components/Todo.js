import React,{useState, useContext} from 'react'
import {Context} from "./App.js"

function Todo({values,info}) {
    const allData = useContext(Context)
   

    const[displayButtons,setDisplayButtons]= useState({
        saveButton: false,
        editButton: true,
        checkbox: true,
        deleteButton:true,
        value:"",
    })

 

    const toggleCheckbox =(e)=>{
      let array = JSON.parse(localStorage.getItem("todos"));
      let counter = 0;
      array.forEach((element)=>{
       
        if(element.value === values.value && element.id === values.id){
  
            element.checked = !element.checked
        
        }
      })
    
      localStorage.setItem("todos",JSON.stringify(array))
      
    }
  return (
    <div className="todoOuterContainer">
      <div className="todoInnerContainer">
        <p className={values.checked?"strike":"todoPara"} id={values.specificId}  >{values.value}</p>
        {displayButtons.checkbox && <input className='checkboxInput' type="checkbox" checked={values.checked? true:false} onChange={toggleCheckbox}/>}
        {displayButtons.editButton && <button className="button" onClick={(e)=>{info.editData(e,setDisplayButtons,values.value)}}>edit</button>}
        {displayButtons.deleteButton && <button className="button" onClick={allData.deleteTodo}>delete</button>}
        {displayButtons.saveButton && <button onClick={(e=>{ allData.save(e,setDisplayButtons) })} className="button">save</button>}
        </div>
    </div>
  )
}

export default Todo