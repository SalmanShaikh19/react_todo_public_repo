import TodoHeader from "./TodoHeader";
import React,{useState, createContext , useEffect} from 'react'
import "../todo.css"
import TodoBody from "./TodoBody";


const Context = createContext(null)

function App() {
  const[isChecked, setIsChecked] = useState(false);

  const[data,setData]=useState({
    changingInput:"",
    array:[],
    indexOfEditedElement:null,
    updatedArray:[],
   
  })

  const addToLocalStorage = (e)=>{

   if(data.changingInput){
    if(localStorage.getItem("todos")){
      let array = JSON.parse(localStorage.getItem("todos"))
      array.push({value:data.changingInput,checked:false,id: Math.random()});
      localStorage.setItem("todos", JSON.stringify(array))
    
    }else{
      localStorage.setItem("todos",JSON.stringify([{value:data.changingInput,checked:false,id: Math.random()}]));
    }
   }else{
     alert("please fill the field")
   }
  
  }

  const renderUI =()=>{
    if(localStorage.getItem("todos")){
    
      let array = JSON.parse(localStorage.getItem("todos"))
      setData((previousValues)=>{
        return{
          ...previousValues,
          array: array,
        }
      })
    }
  }

  

  const save = (e,setDisplayButtons)=>{
   
    setDisplayButtons((previousValues)=>{
      return{
        ...previousValues,
        saveButton: false,
        editButton: true,
        checkbox: true,
        deleteButton:true,
      }
    })

    let editedData = e.target.parentElement.firstChild.innerText
    e.target.parentElement.firstChild.contentEditable = false;
    let array = Array.from(JSON.parse(localStorage.getItem("todos")))
 
    array.forEach((element,index)=>{
      if(index === data.indexOfEditedElement){
        array[index]["value"]= editedData
      }
    })

    //saving data to localStorage
    localStorage.setItem("todos", JSON.stringify(array))
  }

  const editData = (e,setDisplayButtons)=>{
    setDisplayButtons((previousValues)=>{
      return{
        ...previousValues,
        saveButton: true,
        editButton: false,
        checkbox: false,
        deleteButton:false,
      }
    })
  

    e.target.previousElementSibling.previousElementSibling.contentEditable = true;
    let array = Array.from(JSON.parse(localStorage.getItem("todos")));

 
  
   let indexofUpdatedElement = null;
  
    array.forEach((element,index)=>{
      if(element.value === e.target.previousElementSibling.previousElementSibling.innerText ){
       
        indexofUpdatedElement = index
      }
    })

    setData((previousValues)=>{
      return{
        ...previousValues,
        indexOfEditedElement: indexofUpdatedElement,
       
      }
    })
    
   
  }


  const deleteTodo =(e)=>{
    let array = Array.from(JSON.parse(localStorage.getItem("todos")));
    let indexOfDeletedElement =null;
    array.forEach((element,index)=>{
      if(element.value === e.target.parentElement.firstChild.innerText){
          indexOfDeletedElement = index;
      }
    })
    array.splice(indexOfDeletedElement,1)
   
    localStorage.setItem("todos",JSON.stringify(array))
   
  }

  useEffect(()=>{
 
    renderUI()
  },[editData,deleteTodo,addToLocalStorage])
 
  return (
    <Context.Provider value={{
      isChecked:isChecked,
      setIsChecked:setIsChecked,
      deleteTodo: deleteTodo,
      data: data,
      setData:setData,
      save: save
    }}>
    <div className="App">

       <h1 className="todo__heading">To-Do</h1>
       <TodoHeader info={{
         data: data,
         setData:setData,
         addToLocalStorage: addToLocalStorage
       }}/>

        <TodoBody info={{
          data:data,
          setData:setData,
          editData:editData,
        }}/>
    </div>
    </Context.Provider>
  );
}

export {App,Context};












