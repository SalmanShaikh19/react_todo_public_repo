# Todo in React


# Deployed Link

[deployed link](https://react-todo-public-repo.vercel.app/)



# Structure and Working


1. public
    * It contains single `index.html` file which is having a single container with id root

2. src
    1. index.js 
     * This file mounts the main component `App.js` onto the root element in `index.html` file

    2. todo.css
       * All css for the project is written inside todo.css
    
    3. components
       * App.js is having two child components TodoBody.js and TodoHeader.js
       * Todo.js is child component of TodoBody.js
    

## Setup the project

Run this command to install dependencies 
   
    npm install   

Now run

    npm start

This will start the live server


